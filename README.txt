
Module: Hold on, Stranger!
Author: K2C <http://drupal.org/user/3327293>


Description
===========
Adds the Holdonstranger exit popups to your website.

Requirements
============

* Holdonstranger user account

Installation
============
Copy the 'holdonstranger' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
In the settings page enter your Holdonstranger account email and password.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.
Login into your holdonstranger account, and you will see that your
website was automatically added to the Your Websites list.
